﻿namespace GymAppWF.Forms.UCs
{
    partial class NewClientControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.MainPanel = new MetroFramework.Controls.MetroPanel();
            this.FormPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ClientCountryTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.MsgLbl = new MetroFramework.Controls.MetroLabel();
            this.AddClientBtn = new MetroFramework.Controls.MetroTile();
            this.ClientPostCodeTxt = new MetroFramework.Controls.MetroTextBox();
            this.ClientCityTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.ClientApartNumTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.ClientHouseNumTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.ClientStreetTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.ClientPeselTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.ClientPhoneTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.ClientMailTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.ClientSurnameTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.ClientNameTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.MainPanel.SuspendLayout();
            this.FormPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.FormPanel);
            this.MainPanel.Controls.Add(this.metroLabel6);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.HorizontalScrollbarBarColor = true;
            this.MainPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.MainPanel.HorizontalScrollbarSize = 10;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(636, 557);
            this.MainPanel.TabIndex = 1;
            this.MainPanel.VerticalScrollbarBarColor = true;
            this.MainPanel.VerticalScrollbarHighlightOnWheel = false;
            this.MainPanel.VerticalScrollbarSize = 10;
            // 
            // FormPanel
            // 
            this.FormPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FormPanel.ColumnCount = 2;
            this.FormPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.FormPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.FormPanel.Controls.Add(this.ClientCountryTxt, 1, 10);
            this.FormPanel.Controls.Add(this.metroLabel12, 0, 10);
            this.FormPanel.Controls.Add(this.MsgLbl, 0, 11);
            this.FormPanel.Controls.Add(this.AddClientBtn, 1, 11);
            this.FormPanel.Controls.Add(this.ClientPostCodeTxt, 1, 9);
            this.FormPanel.Controls.Add(this.ClientCityTxt, 1, 8);
            this.FormPanel.Controls.Add(this.metroLabel7, 0, 9);
            this.FormPanel.Controls.Add(this.ClientApartNumTxt, 1, 7);
            this.FormPanel.Controls.Add(this.metroLabel2, 0, 1);
            this.FormPanel.Controls.Add(this.ClientHouseNumTxt, 1, 6);
            this.FormPanel.Controls.Add(this.metroLabel8, 0, 8);
            this.FormPanel.Controls.Add(this.ClientStreetTxt, 1, 5);
            this.FormPanel.Controls.Add(this.metroLabel3, 0, 2);
            this.FormPanel.Controls.Add(this.ClientPeselTxt, 1, 4);
            this.FormPanel.Controls.Add(this.metroLabel9, 0, 7);
            this.FormPanel.Controls.Add(this.ClientPhoneTxt, 1, 3);
            this.FormPanel.Controls.Add(this.metroLabel4, 0, 3);
            this.FormPanel.Controls.Add(this.ClientMailTxt, 1, 2);
            this.FormPanel.Controls.Add(this.metroLabel10, 0, 6);
            this.FormPanel.Controls.Add(this.ClientSurnameTxt, 1, 1);
            this.FormPanel.Controls.Add(this.metroLabel5, 0, 4);
            this.FormPanel.Controls.Add(this.ClientNameTxt, 1, 0);
            this.FormPanel.Controls.Add(this.metroLabel11, 0, 5);
            this.FormPanel.Controls.Add(this.metroLabel1, 0, 0);
            this.FormPanel.Location = new System.Drawing.Point(33, 71);
            this.FormPanel.Name = "FormPanel";
            this.FormPanel.RowCount = 12;
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946742F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.945948F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.945948F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.58741F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.FormPanel.Size = new System.Drawing.Size(555, 442);
            this.FormPanel.TabIndex = 21;
            // 
            // ClientCountryTxt
            // 
            this.ClientCountryTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientCountryTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientCountryTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientCountryTxt.Lines = new string[0];
            this.ClientCountryTxt.Location = new System.Drawing.Point(280, 353);
            this.ClientCountryTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientCountryTxt.MaxLength = 32767;
            this.ClientCountryTxt.Name = "ClientCountryTxt";
            this.ClientCountryTxt.PasswordChar = '\0';
            this.ClientCountryTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientCountryTxt.SelectedText = "";
            this.ClientCountryTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientCountryTxt.TabIndex = 67;
            this.ClientCountryTxt.UseCustomBackColor = true;
            this.ClientCountryTxt.UseCustomForeColor = true;
            this.ClientCountryTxt.UseSelectable = true;
            // 
            // metroLabel12
            // 
            this.metroLabel12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel12.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel12.Location = new System.Drawing.Point(3, 350);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(271, 35);
            this.metroLabel12.TabIndex = 66;
            this.metroLabel12.Text = "Kraj:";
            this.metroLabel12.UseCustomBackColor = true;
            this.metroLabel12.UseCustomForeColor = true;
            // 
            // MsgLbl
            // 
            this.MsgLbl.AutoSize = true;
            this.MsgLbl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.MsgLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MsgLbl.ForeColor = System.Drawing.Color.Salmon;
            this.MsgLbl.Location = new System.Drawing.Point(3, 385);
            this.MsgLbl.Name = "MsgLbl";
            this.MsgLbl.Size = new System.Drawing.Size(271, 57);
            this.MsgLbl.TabIndex = 22;
            this.MsgLbl.UseCustomForeColor = true;
            this.MsgLbl.Visible = false;
            // 
            // AddClientBtn
            // 
            this.AddClientBtn.ActiveControl = null;
            this.AddClientBtn.BackColor = System.Drawing.Color.Indigo;
            this.AddClientBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddClientBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.AddClientBtn.Location = new System.Drawing.Point(280, 388);
            this.AddClientBtn.Name = "AddClientBtn";
            this.AddClientBtn.Size = new System.Drawing.Size(272, 51);
            this.AddClientBtn.TabIndex = 22;
            this.AddClientBtn.Text = "Dodaj Klienta";
            this.AddClientBtn.UseCustomBackColor = true;
            this.AddClientBtn.UseCustomForeColor = true;
            this.AddClientBtn.UseSelectable = true;
            this.AddClientBtn.Click += new System.EventHandler(this.AddClientBtn_Click);
            // 
            // ClientPostCodeTxt
            // 
            this.ClientPostCodeTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientPostCodeTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientPostCodeTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientPostCodeTxt.Lines = new string[0];
            this.ClientPostCodeTxt.Location = new System.Drawing.Point(280, 318);
            this.ClientPostCodeTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientPostCodeTxt.MaxLength = 32767;
            this.ClientPostCodeTxt.Name = "ClientPostCodeTxt";
            this.ClientPostCodeTxt.PasswordChar = '\0';
            this.ClientPostCodeTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientPostCodeTxt.SelectedText = "";
            this.ClientPostCodeTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientPostCodeTxt.TabIndex = 66;
            this.ClientPostCodeTxt.UseCustomBackColor = true;
            this.ClientPostCodeTxt.UseCustomForeColor = true;
            this.ClientPostCodeTxt.UseSelectable = true;
            // 
            // ClientCityTxt
            // 
            this.ClientCityTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientCityTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientCityTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientCityTxt.Lines = new string[0];
            this.ClientCityTxt.Location = new System.Drawing.Point(280, 283);
            this.ClientCityTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientCityTxt.MaxLength = 32767;
            this.ClientCityTxt.Name = "ClientCityTxt";
            this.ClientCityTxt.PasswordChar = '\0';
            this.ClientCityTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientCityTxt.SelectedText = "";
            this.ClientCityTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientCityTxt.TabIndex = 65;
            this.ClientCityTxt.UseCustomBackColor = true;
            this.ClientCityTxt.UseCustomForeColor = true;
            this.ClientCityTxt.UseSelectable = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel7.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel7.Location = new System.Drawing.Point(3, 315);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(271, 35);
            this.metroLabel7.TabIndex = 65;
            this.metroLabel7.Text = "Kod pocztowy:";
            this.metroLabel7.UseCustomBackColor = true;
            this.metroLabel7.UseCustomForeColor = true;
            // 
            // ClientApartNumTxt
            // 
            this.ClientApartNumTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientApartNumTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientApartNumTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientApartNumTxt.Lines = new string[0];
            this.ClientApartNumTxt.Location = new System.Drawing.Point(280, 248);
            this.ClientApartNumTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientApartNumTxt.MaxLength = 32767;
            this.ClientApartNumTxt.Name = "ClientApartNumTxt";
            this.ClientApartNumTxt.PasswordChar = '\0';
            this.ClientApartNumTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientApartNumTxt.SelectedText = "";
            this.ClientApartNumTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientApartNumTxt.TabIndex = 64;
            this.ClientApartNumTxt.UseCustomBackColor = true;
            this.ClientApartNumTxt.UseCustomForeColor = true;
            this.ClientApartNumTxt.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel2.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel2.Location = new System.Drawing.Point(3, 35);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(271, 35);
            this.metroLabel2.TabIndex = 57;
            this.metroLabel2.Text = "Nazwisko:";
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // ClientHouseNumTxt
            // 
            this.ClientHouseNumTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientHouseNumTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientHouseNumTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientHouseNumTxt.Lines = new string[0];
            this.ClientHouseNumTxt.Location = new System.Drawing.Point(280, 213);
            this.ClientHouseNumTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientHouseNumTxt.MaxLength = 32767;
            this.ClientHouseNumTxt.Name = "ClientHouseNumTxt";
            this.ClientHouseNumTxt.PasswordChar = '\0';
            this.ClientHouseNumTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientHouseNumTxt.SelectedText = "";
            this.ClientHouseNumTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientHouseNumTxt.TabIndex = 63;
            this.ClientHouseNumTxt.UseCustomBackColor = true;
            this.ClientHouseNumTxt.UseCustomForeColor = true;
            this.ClientHouseNumTxt.UseSelectable = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel8.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel8.Location = new System.Drawing.Point(3, 280);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(271, 35);
            this.metroLabel8.TabIndex = 64;
            this.metroLabel8.Text = "Miasto:";
            this.metroLabel8.UseCustomBackColor = true;
            this.metroLabel8.UseCustomForeColor = true;
            // 
            // ClientStreetTxt
            // 
            this.ClientStreetTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientStreetTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientStreetTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientStreetTxt.Lines = new string[0];
            this.ClientStreetTxt.Location = new System.Drawing.Point(280, 178);
            this.ClientStreetTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientStreetTxt.MaxLength = 32767;
            this.ClientStreetTxt.Name = "ClientStreetTxt";
            this.ClientStreetTxt.PasswordChar = '\0';
            this.ClientStreetTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientStreetTxt.SelectedText = "";
            this.ClientStreetTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientStreetTxt.TabIndex = 62;
            this.ClientStreetTxt.UseCustomBackColor = true;
            this.ClientStreetTxt.UseCustomForeColor = true;
            this.ClientStreetTxt.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel3.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel3.Location = new System.Drawing.Point(3, 70);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(271, 35);
            this.metroLabel3.TabIndex = 58;
            this.metroLabel3.Text = "E-Mail";
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // ClientPeselTxt
            // 
            this.ClientPeselTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientPeselTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientPeselTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientPeselTxt.Lines = new string[0];
            this.ClientPeselTxt.Location = new System.Drawing.Point(280, 143);
            this.ClientPeselTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientPeselTxt.MaxLength = 32767;
            this.ClientPeselTxt.Name = "ClientPeselTxt";
            this.ClientPeselTxt.PasswordChar = '\0';
            this.ClientPeselTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientPeselTxt.SelectedText = "";
            this.ClientPeselTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientPeselTxt.TabIndex = 61;
            this.ClientPeselTxt.UseCustomBackColor = true;
            this.ClientPeselTxt.UseCustomForeColor = true;
            this.ClientPeselTxt.UseSelectable = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel9.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel9.Location = new System.Drawing.Point(3, 245);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(271, 35);
            this.metroLabel9.TabIndex = 63;
            this.metroLabel9.Text = "Nr. mieszkania";
            this.metroLabel9.UseCustomBackColor = true;
            this.metroLabel9.UseCustomForeColor = true;
            // 
            // ClientPhoneTxt
            // 
            this.ClientPhoneTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientPhoneTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientPhoneTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientPhoneTxt.Lines = new string[0];
            this.ClientPhoneTxt.Location = new System.Drawing.Point(280, 108);
            this.ClientPhoneTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientPhoneTxt.MaxLength = 32767;
            this.ClientPhoneTxt.Name = "ClientPhoneTxt";
            this.ClientPhoneTxt.PasswordChar = '\0';
            this.ClientPhoneTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientPhoneTxt.SelectedText = "";
            this.ClientPhoneTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientPhoneTxt.TabIndex = 60;
            this.ClientPhoneTxt.UseCustomBackColor = true;
            this.ClientPhoneTxt.UseCustomForeColor = true;
            this.ClientPhoneTxt.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel4.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel4.Location = new System.Drawing.Point(3, 105);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(271, 35);
            this.metroLabel4.TabIndex = 59;
            this.metroLabel4.Text = "Telefon kontaktowy:";
            this.metroLabel4.UseCustomBackColor = true;
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // ClientMailTxt
            // 
            this.ClientMailTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientMailTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientMailTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientMailTxt.Lines = new string[0];
            this.ClientMailTxt.Location = new System.Drawing.Point(280, 73);
            this.ClientMailTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientMailTxt.MaxLength = 32767;
            this.ClientMailTxt.Name = "ClientMailTxt";
            this.ClientMailTxt.PasswordChar = '\0';
            this.ClientMailTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientMailTxt.SelectedText = "";
            this.ClientMailTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientMailTxt.TabIndex = 59;
            this.ClientMailTxt.UseCustomBackColor = true;
            this.ClientMailTxt.UseCustomForeColor = true;
            this.ClientMailTxt.UseSelectable = true;
            // 
            // metroLabel10
            // 
            this.metroLabel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel10.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel10.Location = new System.Drawing.Point(3, 210);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(271, 35);
            this.metroLabel10.TabIndex = 62;
            this.metroLabel10.Text = "Nr. domu";
            this.metroLabel10.UseCustomBackColor = true;
            this.metroLabel10.UseCustomForeColor = true;
            // 
            // ClientSurnameTxt
            // 
            this.ClientSurnameTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientSurnameTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSurnameTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientSurnameTxt.Lines = new string[0];
            this.ClientSurnameTxt.Location = new System.Drawing.Point(280, 38);
            this.ClientSurnameTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientSurnameTxt.MaxLength = 32767;
            this.ClientSurnameTxt.Name = "ClientSurnameTxt";
            this.ClientSurnameTxt.PasswordChar = '\0';
            this.ClientSurnameTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientSurnameTxt.SelectedText = "";
            this.ClientSurnameTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientSurnameTxt.TabIndex = 58;
            this.ClientSurnameTxt.UseCustomBackColor = true;
            this.ClientSurnameTxt.UseCustomForeColor = true;
            this.ClientSurnameTxt.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel5.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel5.Location = new System.Drawing.Point(3, 140);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(271, 35);
            this.metroLabel5.TabIndex = 60;
            this.metroLabel5.Text = "PESEL";
            this.metroLabel5.UseCustomBackColor = true;
            this.metroLabel5.UseCustomForeColor = true;
            // 
            // ClientNameTxt
            // 
            this.ClientNameTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientNameTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientNameTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientNameTxt.Lines = new string[0];
            this.ClientNameTxt.Location = new System.Drawing.Point(280, 3);
            this.ClientNameTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientNameTxt.MaxLength = 32767;
            this.ClientNameTxt.Name = "ClientNameTxt";
            this.ClientNameTxt.PasswordChar = '\0';
            this.ClientNameTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientNameTxt.SelectedText = "";
            this.ClientNameTxt.Size = new System.Drawing.Size(272, 29);
            this.ClientNameTxt.TabIndex = 57;
            this.ClientNameTxt.UseCustomBackColor = true;
            this.ClientNameTxt.UseCustomForeColor = true;
            this.ClientNameTxt.UseSelectable = true;
            // 
            // metroLabel11
            // 
            this.metroLabel11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel11.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel11.Location = new System.Drawing.Point(3, 175);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(271, 35);
            this.metroLabel11.TabIndex = 61;
            this.metroLabel11.Text = "Ulica:";
            this.metroLabel11.UseCustomBackColor = true;
            this.metroLabel11.UseCustomForeColor = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel1.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(271, 35);
            this.metroLabel1.TabIndex = 56;
            this.metroLabel1.Text = "Imię:";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel6.Location = new System.Drawing.Point(33, 30);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(111, 25);
            this.metroLabel6.TabIndex = 20;
            this.metroLabel6.Text = "Nowy Klient:";
            this.metroLabel6.UseCustomBackColor = true;
            this.metroLabel6.UseCustomForeColor = true;
            // 
            // NewClientControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.MainPanel);
            this.Name = "NewClientControl";
            this.Size = new System.Drawing.Size(636, 557);
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.FormPanel.ResumeLayout(false);
            this.FormPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private MetroFramework.Controls.MetroPanel MainPanel;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private System.Windows.Forms.TableLayoutPanel FormPanel;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroTextBox ClientCountryTxt;
        private MetroFramework.Controls.MetroTextBox ClientPostCodeTxt;
        private MetroFramework.Controls.MetroTextBox ClientCityTxt;
        private MetroFramework.Controls.MetroTextBox ClientApartNumTxt;
        private MetroFramework.Controls.MetroTextBox ClientHouseNumTxt;
        private MetroFramework.Controls.MetroTextBox ClientStreetTxt;
        private MetroFramework.Controls.MetroTextBox ClientPeselTxt;
        private MetroFramework.Controls.MetroTextBox ClientPhoneTxt;
        private MetroFramework.Controls.MetroTextBox ClientMailTxt;
        private MetroFramework.Controls.MetroTextBox ClientSurnameTxt;
        private MetroFramework.Controls.MetroTextBox ClientNameTxt;
        private MetroFramework.Controls.MetroTile AddClientBtn;
        private MetroFramework.Controls.MetroLabel MsgLbl;
    }
}
