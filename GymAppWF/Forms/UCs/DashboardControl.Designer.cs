﻿namespace GymAppWF.Forms.UCs
{
    partial class DashboardControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPanel = new MetroFramework.Controls.MetroPanel();
            this.DashBoardTab = new MetroFramework.Controls.MetroTabControl();
            this.Dashboard = new MetroFramework.Controls.MetroTabPage();
            this.DashPanel = new MetroFramework.Controls.MetroPanel();
            this.SplitContainer = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.BtnTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.SettingBtn = new MetroFramework.Controls.MetroTile();
            this.MngPassBtn = new MetroFramework.Controls.MetroTile();
            this.AddClientBtn = new MetroFramework.Controls.MetroTile();
            this.addPassBtn = new MetroFramework.Controls.MetroTile();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.metroScrollBar1 = new MetroFramework.Controls.MetroScrollBar();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.DynamicPanel = new MetroFramework.Controls.MetroPanel();
            this.ClientTab = new MetroFramework.Controls.MetroTabPage();
            this.ClientPanel = new MetroFramework.Controls.MetroPanel();
            this.PassTab = new MetroFramework.Controls.MetroTabPage();
            this.EntryTab = new MetroFramework.Controls.MetroTabPage();
            this.PassPanel = new MetroFramework.Controls.MetroPanel();
            this.MainPanel.SuspendLayout();
            this.DashBoardTab.SuspendLayout();
            this.Dashboard.SuspendLayout();
            this.DashPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).BeginInit();
            this.SplitContainer.Panel1.SuspendLayout();
            this.SplitContainer.Panel2.SuspendLayout();
            this.SplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.BtnTableLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.SuspendLayout();
            this.ClientTab.SuspendLayout();
            this.PassTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainPanel.Controls.Add(this.DashBoardTab);
            this.MainPanel.HorizontalScrollbarBarColor = true;
            this.MainPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.MainPanel.HorizontalScrollbarSize = 10;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(1000, 600);
            this.MainPanel.TabIndex = 0;
            this.MainPanel.Theme = MetroFramework.MetroThemeStyle.Light;
            this.MainPanel.VerticalScrollbarBarColor = true;
            this.MainPanel.VerticalScrollbarHighlightOnWheel = false;
            this.MainPanel.VerticalScrollbarSize = 10;
            // 
            // DashBoardTab
            // 
            this.DashBoardTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DashBoardTab.Controls.Add(this.Dashboard);
            this.DashBoardTab.Controls.Add(this.ClientTab);
            this.DashBoardTab.Controls.Add(this.PassTab);
            this.DashBoardTab.Controls.Add(this.EntryTab);
            this.DashBoardTab.FontSize = MetroFramework.MetroTabControlSize.Tall;
            this.DashBoardTab.Location = new System.Drawing.Point(0, 0);
            this.DashBoardTab.Name = "DashBoardTab";
            this.DashBoardTab.SelectedIndex = 2;
            this.DashBoardTab.Size = new System.Drawing.Size(1000, 600);
            this.DashBoardTab.TabIndex = 2;
            this.DashBoardTab.Theme = MetroFramework.MetroThemeStyle.Light;
            this.DashBoardTab.UseSelectable = true;
            this.DashBoardTab.SelectedIndexChanged += new System.EventHandler(this.DashBoardTab_SelectedIndexChanged);
            // 
            // Dashboard
            // 
            this.Dashboard.Controls.Add(this.DashPanel);
            this.Dashboard.HorizontalScrollbarBarColor = true;
            this.Dashboard.HorizontalScrollbarHighlightOnWheel = false;
            this.Dashboard.HorizontalScrollbarSize = 10;
            this.Dashboard.Location = new System.Drawing.Point(4, 44);
            this.Dashboard.Name = "Dashboard";
            this.Dashboard.Size = new System.Drawing.Size(992, 552);
            this.Dashboard.TabIndex = 0;
            this.Dashboard.Text = "Dashboard";
            this.Dashboard.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Dashboard.VerticalScrollbarBarColor = true;
            this.Dashboard.VerticalScrollbarHighlightOnWheel = false;
            this.Dashboard.VerticalScrollbarSize = 10;
            this.Dashboard.Click += new System.EventHandler(this.Dashboard_Click);
            // 
            // DashPanel
            // 
            this.DashPanel.Controls.Add(this.SplitContainer);
            this.DashPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DashPanel.HorizontalScrollbarBarColor = true;
            this.DashPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.DashPanel.HorizontalScrollbarSize = 10;
            this.DashPanel.Location = new System.Drawing.Point(0, 0);
            this.DashPanel.Name = "DashPanel";
            this.DashPanel.Size = new System.Drawing.Size(992, 552);
            this.DashPanel.TabIndex = 2;
            this.DashPanel.VerticalScrollbarBarColor = true;
            this.DashPanel.VerticalScrollbarHighlightOnWheel = false;
            this.DashPanel.VerticalScrollbarSize = 10;
            // 
            // SplitContainer
            // 
            this.SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer.Name = "SplitContainer";
            // 
            // SplitContainer.Panel1
            // 
            this.SplitContainer.Panel1.BackColor = System.Drawing.SystemColors.Window;
            this.SplitContainer.Panel1.Controls.Add(this.splitContainer1);
            this.SplitContainer.Panel1.Controls.Add(this.splitter1);
            // 
            // SplitContainer.Panel2
            // 
            this.SplitContainer.Panel2.BackColor = System.Drawing.Color.White;
            this.SplitContainer.Panel2.Controls.Add(this.DynamicPanel);
            this.SplitContainer.Size = new System.Drawing.Size(992, 552);
            this.SplitContainer.SplitterDistance = 352;
            this.SplitContainer.TabIndex = 2;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.BtnTableLayout);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2.Controls.Add(this.metroScrollBar1);
            this.splitContainer1.Size = new System.Drawing.Size(349, 552);
            this.splitContainer1.SplitterDistance = 256;
            this.splitContainer1.TabIndex = 2;
            // 
            // BtnTableLayout
            // 
            this.BtnTableLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnTableLayout.ColumnCount = 3;
            this.BtnTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.BtnTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.BtnTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.BtnTableLayout.Controls.Add(this.SettingBtn, 0, 1);
            this.BtnTableLayout.Controls.Add(this.MngPassBtn, 2, 0);
            this.BtnTableLayout.Controls.Add(this.AddClientBtn, 0, 0);
            this.BtnTableLayout.Controls.Add(this.addPassBtn, 1, 0);
            this.BtnTableLayout.Location = new System.Drawing.Point(20, 28);
            this.BtnTableLayout.Name = "BtnTableLayout";
            this.BtnTableLayout.RowCount = 2;
            this.BtnTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.BtnTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.BtnTableLayout.Size = new System.Drawing.Size(308, 200);
            this.BtnTableLayout.TabIndex = 0;
            // 
            // SettingBtn
            // 
            this.SettingBtn.ActiveControl = null;
            this.SettingBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SettingBtn.Location = new System.Drawing.Point(3, 103);
            this.SettingBtn.MaximumSize = new System.Drawing.Size(200, 200);
            this.SettingBtn.Name = "SettingBtn";
            this.SettingBtn.Size = new System.Drawing.Size(96, 94);
            this.SettingBtn.TabIndex = 6;
            this.SettingBtn.Text = "Ustawienia";
            this.SettingBtn.UseSelectable = true;
            // 
            // MngPassBtn
            // 
            this.MngPassBtn.ActiveControl = null;
            this.MngPassBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MngPassBtn.Location = new System.Drawing.Point(207, 3);
            this.MngPassBtn.MaximumSize = new System.Drawing.Size(200, 200);
            this.MngPassBtn.Name = "MngPassBtn";
            this.MngPassBtn.Size = new System.Drawing.Size(98, 94);
            this.MngPassBtn.TabIndex = 7;
            this.MngPassBtn.Text = "Definicje karnetów";
            this.MngPassBtn.UseSelectable = true;
            // 
            // AddClientBtn
            // 
            this.AddClientBtn.ActiveControl = null;
            this.AddClientBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddClientBtn.Location = new System.Drawing.Point(3, 3);
            this.AddClientBtn.MaximumSize = new System.Drawing.Size(200, 200);
            this.AddClientBtn.Name = "AddClientBtn";
            this.AddClientBtn.Size = new System.Drawing.Size(96, 94);
            this.AddClientBtn.TabIndex = 4;
            this.AddClientBtn.Text = "Nowy Klient";
            this.AddClientBtn.UseSelectable = true;
            this.AddClientBtn.Click += new System.EventHandler(this.AddClientBtn_Click);
            // 
            // addPassBtn
            // 
            this.addPassBtn.ActiveControl = null;
            this.addPassBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addPassBtn.Location = new System.Drawing.Point(105, 3);
            this.addPassBtn.MaximumSize = new System.Drawing.Size(200, 200);
            this.addPassBtn.Name = "addPassBtn";
            this.addPassBtn.Size = new System.Drawing.Size(96, 94);
            this.addPassBtn.TabIndex = 5;
            this.addPassBtn.Text = "Nowy Karnet";
            this.addPassBtn.UseSelectable = true;
            this.addPassBtn.Click += new System.EventHandler(this.addPassBtn_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Panel1Collapsed = true;
            this.splitContainer2.Size = new System.Drawing.Size(349, 292);
            this.splitContainer2.SplitterDistance = 116;
            this.splitContainer2.TabIndex = 1;
            // 
            // metroScrollBar1
            // 
            this.metroScrollBar1.LargeChange = 10;
            this.metroScrollBar1.Location = new System.Drawing.Point(0, 0);
            this.metroScrollBar1.Maximum = 100;
            this.metroScrollBar1.Minimum = 0;
            this.metroScrollBar1.MouseWheelBarPartitions = 10;
            this.metroScrollBar1.Name = "metroScrollBar1";
            this.metroScrollBar1.Orientation = MetroFramework.Controls.MetroScrollOrientation.Vertical;
            this.metroScrollBar1.ScrollbarSize = 10;
            this.metroScrollBar1.Size = new System.Drawing.Size(10, 200);
            this.metroScrollBar1.TabIndex = 0;
            this.metroScrollBar1.UseSelectable = true;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 552);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // DynamicPanel
            // 
            this.DynamicPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DynamicPanel.HorizontalScrollbarBarColor = true;
            this.DynamicPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.DynamicPanel.HorizontalScrollbarSize = 10;
            this.DynamicPanel.Location = new System.Drawing.Point(0, 0);
            this.DynamicPanel.Name = "DynamicPanel";
            this.DynamicPanel.Size = new System.Drawing.Size(636, 552);
            this.DynamicPanel.TabIndex = 0;
            this.DynamicPanel.VerticalScrollbarBarColor = true;
            this.DynamicPanel.VerticalScrollbarHighlightOnWheel = false;
            this.DynamicPanel.VerticalScrollbarSize = 10;
            // 
            // ClientTab
            // 
            this.ClientTab.Controls.Add(this.ClientPanel);
            this.ClientTab.HorizontalScrollbarBarColor = true;
            this.ClientTab.HorizontalScrollbarHighlightOnWheel = false;
            this.ClientTab.HorizontalScrollbarSize = 10;
            this.ClientTab.Location = new System.Drawing.Point(4, 44);
            this.ClientTab.Name = "ClientTab";
            this.ClientTab.Size = new System.Drawing.Size(992, 552);
            this.ClientTab.TabIndex = 1;
            this.ClientTab.Text = "Klienci";
            this.ClientTab.VerticalScrollbarBarColor = true;
            this.ClientTab.VerticalScrollbarHighlightOnWheel = false;
            this.ClientTab.VerticalScrollbarSize = 10;
            // 
            // ClientPanel
            // 
            this.ClientPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientPanel.HorizontalScrollbarBarColor = true;
            this.ClientPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.ClientPanel.HorizontalScrollbarSize = 10;
            this.ClientPanel.Location = new System.Drawing.Point(0, 0);
            this.ClientPanel.Name = "ClientPanel";
            this.ClientPanel.Size = new System.Drawing.Size(992, 552);
            this.ClientPanel.TabIndex = 2;
            this.ClientPanel.VerticalScrollbarBarColor = true;
            this.ClientPanel.VerticalScrollbarHighlightOnWheel = false;
            this.ClientPanel.VerticalScrollbarSize = 10;
            this.ClientPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ClientPanel_Paint);
            // 
            // PassTab
            // 
            this.PassTab.Controls.Add(this.PassPanel);
            this.PassTab.HorizontalScrollbarBarColor = true;
            this.PassTab.HorizontalScrollbarHighlightOnWheel = false;
            this.PassTab.HorizontalScrollbarSize = 10;
            this.PassTab.Location = new System.Drawing.Point(4, 44);
            this.PassTab.Name = "PassTab";
            this.PassTab.Size = new System.Drawing.Size(992, 552);
            this.PassTab.TabIndex = 2;
            this.PassTab.Text = "Karnety";
            this.PassTab.VerticalScrollbarBarColor = true;
            this.PassTab.VerticalScrollbarHighlightOnWheel = false;
            this.PassTab.VerticalScrollbarSize = 10;
            // 
            // EntryTab
            // 
            this.EntryTab.HorizontalScrollbarBarColor = true;
            this.EntryTab.HorizontalScrollbarHighlightOnWheel = false;
            this.EntryTab.HorizontalScrollbarSize = 10;
            this.EntryTab.Location = new System.Drawing.Point(4, 44);
            this.EntryTab.Name = "EntryTab";
            this.EntryTab.Size = new System.Drawing.Size(992, 552);
            this.EntryTab.TabIndex = 3;
            this.EntryTab.Text = "Wejścia";
            this.EntryTab.VerticalScrollbarBarColor = true;
            this.EntryTab.VerticalScrollbarHighlightOnWheel = false;
            this.EntryTab.VerticalScrollbarSize = 10;
            // 
            // PassPanel
            // 
            this.PassPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PassPanel.HorizontalScrollbarBarColor = true;
            this.PassPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.PassPanel.HorizontalScrollbarSize = 10;
            this.PassPanel.Location = new System.Drawing.Point(0, 0);
            this.PassPanel.Name = "PassPanel";
            this.PassPanel.Size = new System.Drawing.Size(992, 552);
            this.PassPanel.TabIndex = 3;
            this.PassPanel.VerticalScrollbarBarColor = true;
            this.PassPanel.VerticalScrollbarHighlightOnWheel = false;
            this.PassPanel.VerticalScrollbarSize = 10;
            // 
            // DashboardControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainPanel);
            this.Name = "DashboardControl";
            this.Size = new System.Drawing.Size(1000, 600);
            this.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Load += new System.EventHandler(this.DashboardControl_Load);
            this.MainPanel.ResumeLayout(false);
            this.DashBoardTab.ResumeLayout(false);
            this.Dashboard.ResumeLayout(false);
            this.DashPanel.ResumeLayout(false);
            this.SplitContainer.Panel1.ResumeLayout(false);
            this.SplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).EndInit();
            this.SplitContainer.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.BtnTableLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ClientTab.ResumeLayout(false);
            this.PassTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel MainPanel;
        private MetroFramework.Controls.MetroTabControl DashBoardTab;
        private MetroFramework.Controls.MetroTabPage Dashboard;
        private MetroFramework.Controls.MetroTabPage ClientTab;
        private MetroFramework.Controls.MetroTabPage PassTab;
        private MetroFramework.Controls.MetroTabPage EntryTab;
        private MetroFramework.Controls.MetroPanel DashPanel;
        private System.Windows.Forms.SplitContainer SplitContainer;
        private System.Windows.Forms.TableLayoutPanel BtnTableLayout;
        private MetroFramework.Controls.MetroTile SettingBtn;
        private MetroFramework.Controls.MetroTile MngPassBtn;
        private MetroFramework.Controls.MetroTile AddClientBtn;
        private MetroFramework.Controls.MetroTile addPassBtn;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Splitter splitter1;
        private MetroFramework.Controls.MetroScrollBar metroScrollBar1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private MetroFramework.Controls.MetroPanel DynamicPanel;
        private MetroFramework.Controls.MetroPanel ClientPanel;
        private MetroFramework.Controls.MetroPanel PassPanel;
    }
}
