SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		LJedrzynski
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE deleteClient
	--PERSON
	@FirstName nvarchar(20),
	@LastName nvarchar(40),
	@Email nvarchar(254),
	@Telephone nvarchar(12),
	@Pesel nvarchar(11),
	@Msg nvarchar(255) OUTPUT,
	@Err smallint OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
	DECLARE @PersonId int

	SELECT TOP 1 @PersonId = Id
	FROM dbo.PersonSet 
	WHERE @FirstName = FirstName AND
	LastName = @LastName AND
	(Email = @Email	
	OR Pesel = @Pesel
	OR Telephone = @Telephone)

	IF @PersonId IS NOT NULL
	BEGIN
		DELETE 
		FROM PersonSet_Client 
		WHERE Id = @PersonId

		DELETE 
		FROM PersonSet
		WHERE Id = @PersonId
	END

	SET @Msg = 'U�ytkownik zosta� usuni�ty!'
	SET @Err = 0
	COMMIT TRANSACTION
END
GO