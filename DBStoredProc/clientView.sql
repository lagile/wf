ALTER VIEW clientView
AS
SELECT	dbo.PersonSet.Id, 
		dbo.PersonSet.FirstName AS Imi�, 
		dbo.PersonSet.LastName AS Nazwisko, 
        dbo.PersonSet.Email AS Email, 
		dbo.PersonSet.Telephone AS Telefon, 
		dbo.PersonSet.PESEL AS PESEL, 
		dbo.PersonSet_Client.SignUpDate AS [Data rejestracji], 
		dbo.PersonSet_Client.SignOffDate AS [Data wypisania], 
		dbo.PersonSet_Client.VIP ,
		dbo.AddressSet.Street AS Ulica, 
		dbo.AddressSet.City AS Miasto, 
		dbo.AddressSet.Country AS Kraj, 
		dbo.AddressSet.PostCode AS [Kod pocztowy], 
		dbo.AddressSet.HouseNum AS [Nr. domu], 
		dbo.AddressSet.ApartmentNum AS [Nr. mieszkania]
FROM dbo.AddressSet INNER JOIN
dbo.PersonSet ON dbo.AddressSet.Id = dbo.PersonSet.Person2Address INNER JOIN
dbo.PersonSet_Client ON dbo.PersonSet.Id = dbo.PersonSet_Client.Id