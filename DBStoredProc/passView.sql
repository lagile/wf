ALTER VIEW passView
AS
SELECT	dbo.PassDefSet.Name AS [Nazwa karnetu], 
		dbo.PassSet.StartDate AS [Data startu karnetu], 
		dbo.PassSet.ExpDate AS [Data wa�no�ci], 
		dbo.PassSet.PassId AS [Identyfikator karnetu],
		dbo.PersonSet.FirstName, 
		dbo.PersonSet.LastName,
		dbo.PersonSet.Email, 
		dbo.PersonSet.Telephone,
		dbo.PersonSet.PESEL,
		dbo.PersonSet.Id,
		dbo.PassSet.Valid AS Wa�ny,
		dbo.PassDefSet.Price AS Cena
FROM     dbo.PassDefSet INNER JOIN
         dbo.PassSet ON dbo.PassDefSet.Id = dbo.PassSet.Pass2Def
		  INNER JOIN
         dbo.PersonSet ON dbo.PassSet.Pass2Client = dbo.PersonSet.Id