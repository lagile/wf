﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace GymAppWF.Forms.UCs
{

    public partial class PassControl : UserControl
    {
        private DataSet PassDataSet;

        public PassControl()
        {
            InitializeComponent();
        }

        private void LoadGrid(String query)
        {
            Int16 err = 0;
            String msg = String.Empty;

            PassDataGrid.AutoGenerateColumns = true;
            PassDataSet = ConnectionManager.ExecuteQuery(query, "PassView", ref err, ref msg);
            PassDataGrid.DataSource = PassDataSet.Tables[0];
            PassDataGrid.Columns[0].Visible = false;

            if (err < 0)
            {
                MetroMessageBox.Show(this.ParentForm, msg, "Upps!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PassTabControl_Load(object sender, EventArgs e)
        {
            LoadGrid("SELECT * FROM passView");
        }

        private void OperationPassBtn_Click(object sender, EventArgs e)
        {
            Int16 err = 0;
            String msg = String.Empty;
        }



        private void SearchClientBtn_Click(object sender, EventArgs e)
        {
            Int16 err = 0;
            String msg = String.Empty, query = "SELECT * FROM passView WHERE 1 = 1 ";

            if (!String.IsNullOrEmpty(ClientNameSrchTxt.Text))
            {
                query += " AND FirstName = '" + ClientNameSrchTxt.Text + "'";
            }
            if (!String.IsNullOrEmpty(ClientSurnameSrchTxt.Text))
            {
                query += " AND LastName = '" + ClientSurnameSrchTxt.Text + "'";
            }
            if (!String.IsNullOrEmpty(ClientMailSrchTxt.Text))
            {
                query += " AND Email = '" + ClientMailSrchTxt.Text + "'";
            }
            if (!String.IsNullOrEmpty(ClientPhoneSrchTxt.Text))
            {
                query += " AND Telephone = '" + ClientPhoneSrchTxt.Text + "'";
            }
            if (!String.IsNullOrEmpty(ClientPhoneSrchTxt.Text))
            {
                query += " AND PESEL = '" + ClientPeselSrchTxt.Text + "'";
            }

            LoadGrid(query);

            if (err < 0)
            {
                MetroMessageBox.Show(this.ParentForm, msg, "Upps!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
