﻿namespace GymAppWF.Forms.UCs
{
    partial class ClientControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ClientTabTable = new System.Windows.Forms.TableLayoutPanel();
            this.ClientDataGrid = new System.Windows.Forms.DataGridView();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.DownSplitContainer = new System.Windows.Forms.SplitContainer();
            this.BtnTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.SearchClientBtn = new MetroFramework.Controls.MetroTile();
            this.OperationClientBtn = new MetroFramework.Controls.MetroTile();
            this.FormPanel = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.ClientPeselSrchTxt = new MetroFramework.Controls.MetroTextBox();
            this.ClientPhoneSrchTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.ClientMailSrchTxt = new MetroFramework.Controls.MetroTextBox();
            this.ClientSurnameSrchTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.ClientNameSrchTxt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.ClientTabTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClientDataGrid)).BeginInit();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DownSplitContainer)).BeginInit();
            this.DownSplitContainer.Panel1.SuspendLayout();
            this.DownSplitContainer.Panel2.SuspendLayout();
            this.DownSplitContainer.SuspendLayout();
            this.BtnTableLayout.SuspendLayout();
            this.FormPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ClientTabTable
            // 
            this.ClientTabTable.ColumnCount = 1;
            this.ClientTabTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ClientTabTable.Controls.Add(this.ClientDataGrid, 0, 0);
            this.ClientTabTable.Controls.Add(this.metroPanel1, 2, 1);
            this.ClientTabTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientTabTable.Location = new System.Drawing.Point(0, 0);
            this.ClientTabTable.Name = "ClientTabTable";
            this.ClientTabTable.RowCount = 2;
            this.ClientTabTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55.7971F));
            this.ClientTabTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.2029F));
            this.ClientTabTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ClientTabTable.Size = new System.Drawing.Size(992, 552);
            this.ClientTabTable.TabIndex = 0;
            // 
            // ClientDataGrid
            // 
            this.ClientDataGrid.AllowUserToOrderColumns = true;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Indigo;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Indigo;
            this.ClientDataGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.ClientDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientDataGrid.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Indigo;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ClientDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.ClientDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Indigo;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Indigo;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ClientDataGrid.DefaultCellStyle = dataGridViewCellStyle7;
            this.ClientDataGrid.GridColor = System.Drawing.Color.MediumPurple;
            this.ClientDataGrid.Location = new System.Drawing.Point(3, 3);
            this.ClientDataGrid.Name = "ClientDataGrid";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Indigo;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Indigo;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ClientDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.ClientDataGrid.RowTemplate.Height = 24;
            this.ClientDataGrid.Size = new System.Drawing.Size(986, 301);
            this.ClientDataGrid.TabIndex = 1;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.DownSplitContainer);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 310);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(986, 239);
            this.metroPanel1.TabIndex = 2;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // DownSplitContainer
            // 
            this.DownSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DownSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.DownSplitContainer.Name = "DownSplitContainer";
            // 
            // DownSplitContainer.Panel1
            // 
            this.DownSplitContainer.Panel1.Controls.Add(this.BtnTableLayout);
            // 
            // DownSplitContainer.Panel2
            // 
            this.DownSplitContainer.Panel2.Controls.Add(this.FormPanel);
            this.DownSplitContainer.Size = new System.Drawing.Size(986, 239);
            this.DownSplitContainer.SplitterDistance = 217;
            this.DownSplitContainer.TabIndex = 2;
            // 
            // BtnTableLayout
            // 
            this.BtnTableLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnTableLayout.ColumnCount = 2;
            this.BtnTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.BtnTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.BtnTableLayout.Controls.Add(this.SearchClientBtn, 0, 0);
            this.BtnTableLayout.Controls.Add(this.OperationClientBtn, 1, 0);
            this.BtnTableLayout.Location = new System.Drawing.Point(26, 35);
            this.BtnTableLayout.Name = "BtnTableLayout";
            this.BtnTableLayout.RowCount = 1;
            this.BtnTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.BtnTableLayout.Size = new System.Drawing.Size(163, 82);
            this.BtnTableLayout.TabIndex = 29;
            // 
            // SearchClientBtn
            // 
            this.SearchClientBtn.ActiveControl = null;
            this.SearchClientBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchClientBtn.Location = new System.Drawing.Point(3, 3);
            this.SearchClientBtn.MaximumSize = new System.Drawing.Size(200, 200);
            this.SearchClientBtn.Name = "SearchClientBtn";
            this.SearchClientBtn.Size = new System.Drawing.Size(75, 76);
            this.SearchClientBtn.TabIndex = 4;
            this.SearchClientBtn.Text = "Wyszukaj";
            this.SearchClientBtn.UseSelectable = true;
            this.SearchClientBtn.Click += new System.EventHandler(this.SearchClientBtn_Click);
            // 
            // OperationClientBtn
            // 
            this.OperationClientBtn.ActiveControl = null;
            this.OperationClientBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OperationClientBtn.Location = new System.Drawing.Point(84, 3);
            this.OperationClientBtn.MaximumSize = new System.Drawing.Size(200, 200);
            this.OperationClientBtn.Name = "OperationClientBtn";
            this.OperationClientBtn.Size = new System.Drawing.Size(76, 76);
            this.OperationClientBtn.TabIndex = 5;
            this.OperationClientBtn.Text = "Usuń";
            this.OperationClientBtn.UseSelectable = true;
            this.OperationClientBtn.Click += new System.EventHandler(this.OperationClientBtn_Click);
            // 
            // FormPanel
            // 
            this.FormPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FormPanel.ColumnCount = 2;
            this.FormPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.FormPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.FormPanel.Controls.Add(this.metroLabel2, 0, 1);
            this.FormPanel.Controls.Add(this.metroLabel3, 0, 2);
            this.FormPanel.Controls.Add(this.ClientPeselSrchTxt, 1, 4);
            this.FormPanel.Controls.Add(this.ClientPhoneSrchTxt, 1, 3);
            this.FormPanel.Controls.Add(this.metroLabel4, 0, 3);
            this.FormPanel.Controls.Add(this.ClientMailSrchTxt, 1, 2);
            this.FormPanel.Controls.Add(this.ClientSurnameSrchTxt, 1, 1);
            this.FormPanel.Controls.Add(this.metroLabel5, 0, 4);
            this.FormPanel.Controls.Add(this.ClientNameSrchTxt, 1, 0);
            this.FormPanel.Controls.Add(this.metroLabel1, 0, 0);
            this.FormPanel.Location = new System.Drawing.Point(44, 35);
            this.FormPanel.Name = "FormPanel";
            this.FormPanel.RowCount = 5;
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946742F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.FormPanel.Size = new System.Drawing.Size(678, 169);
            this.FormPanel.TabIndex = 28;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel2.Location = new System.Drawing.Point(3, 33);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(333, 33);
            this.metroLabel2.TabIndex = 57;
            this.metroLabel2.Text = "Nazwisko:";
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel3.Location = new System.Drawing.Point(3, 66);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(333, 33);
            this.metroLabel3.TabIndex = 58;
            this.metroLabel3.Text = "E-Mail";
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // ClientPeselSrchTxt
            // 
            this.ClientPeselSrchTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientPeselSrchTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientPeselSrchTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientPeselSrchTxt.Lines = new string[0];
            this.ClientPeselSrchTxt.Location = new System.Drawing.Point(342, 135);
            this.ClientPeselSrchTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientPeselSrchTxt.MaxLength = 32767;
            this.ClientPeselSrchTxt.Name = "ClientPeselSrchTxt";
            this.ClientPeselSrchTxt.PasswordChar = '\0';
            this.ClientPeselSrchTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientPeselSrchTxt.SelectedText = "";
            this.ClientPeselSrchTxt.Size = new System.Drawing.Size(333, 31);
            this.ClientPeselSrchTxt.TabIndex = 61;
            this.ClientPeselSrchTxt.UseCustomBackColor = true;
            this.ClientPeselSrchTxt.UseCustomForeColor = true;
            this.ClientPeselSrchTxt.UseSelectable = true;
            // 
            // ClientPhoneSrchTxt
            // 
            this.ClientPhoneSrchTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientPhoneSrchTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientPhoneSrchTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientPhoneSrchTxt.Lines = new string[0];
            this.ClientPhoneSrchTxt.Location = new System.Drawing.Point(342, 102);
            this.ClientPhoneSrchTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientPhoneSrchTxt.MaxLength = 32767;
            this.ClientPhoneSrchTxt.Name = "ClientPhoneSrchTxt";
            this.ClientPhoneSrchTxt.PasswordChar = '\0';
            this.ClientPhoneSrchTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientPhoneSrchTxt.SelectedText = "";
            this.ClientPhoneSrchTxt.Size = new System.Drawing.Size(333, 27);
            this.ClientPhoneSrchTxt.TabIndex = 60;
            this.ClientPhoneSrchTxt.UseCustomBackColor = true;
            this.ClientPhoneSrchTxt.UseCustomForeColor = true;
            this.ClientPhoneSrchTxt.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel4.Location = new System.Drawing.Point(3, 99);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(333, 33);
            this.metroLabel4.TabIndex = 59;
            this.metroLabel4.Text = "Telefon kontaktowy:";
            this.metroLabel4.UseCustomBackColor = true;
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // ClientMailSrchTxt
            // 
            this.ClientMailSrchTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientMailSrchTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientMailSrchTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientMailSrchTxt.Lines = new string[0];
            this.ClientMailSrchTxt.Location = new System.Drawing.Point(342, 69);
            this.ClientMailSrchTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientMailSrchTxt.MaxLength = 32767;
            this.ClientMailSrchTxt.Name = "ClientMailSrchTxt";
            this.ClientMailSrchTxt.PasswordChar = '\0';
            this.ClientMailSrchTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientMailSrchTxt.SelectedText = "";
            this.ClientMailSrchTxt.Size = new System.Drawing.Size(333, 27);
            this.ClientMailSrchTxt.TabIndex = 59;
            this.ClientMailSrchTxt.UseCustomBackColor = true;
            this.ClientMailSrchTxt.UseCustomForeColor = true;
            this.ClientMailSrchTxt.UseSelectable = true;
            // 
            // ClientSurnameSrchTxt
            // 
            this.ClientSurnameSrchTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSurnameSrchTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientSurnameSrchTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientSurnameSrchTxt.Lines = new string[0];
            this.ClientSurnameSrchTxt.Location = new System.Drawing.Point(342, 36);
            this.ClientSurnameSrchTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientSurnameSrchTxt.MaxLength = 32767;
            this.ClientSurnameSrchTxt.Name = "ClientSurnameSrchTxt";
            this.ClientSurnameSrchTxt.PasswordChar = '\0';
            this.ClientSurnameSrchTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientSurnameSrchTxt.SelectedText = "";
            this.ClientSurnameSrchTxt.Size = new System.Drawing.Size(333, 27);
            this.ClientSurnameSrchTxt.TabIndex = 58;
            this.ClientSurnameSrchTxt.UseCustomBackColor = true;
            this.ClientSurnameSrchTxt.UseCustomForeColor = true;
            this.ClientSurnameSrchTxt.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel5.Location = new System.Drawing.Point(3, 132);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(333, 37);
            this.metroLabel5.TabIndex = 60;
            this.metroLabel5.Text = "PESEL";
            this.metroLabel5.UseCustomBackColor = true;
            this.metroLabel5.UseCustomForeColor = true;
            // 
            // ClientNameSrchTxt
            // 
            this.ClientNameSrchTxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientNameSrchTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientNameSrchTxt.ForeColor = System.Drawing.Color.Indigo;
            this.ClientNameSrchTxt.Lines = new string[0];
            this.ClientNameSrchTxt.Location = new System.Drawing.Point(342, 3);
            this.ClientNameSrchTxt.MaximumSize = new System.Drawing.Size(0, 38);
            this.ClientNameSrchTxt.MaxLength = 32767;
            this.ClientNameSrchTxt.Name = "ClientNameSrchTxt";
            this.ClientNameSrchTxt.PasswordChar = '\0';
            this.ClientNameSrchTxt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ClientNameSrchTxt.SelectedText = "";
            this.ClientNameSrchTxt.Size = new System.Drawing.Size(333, 27);
            this.ClientNameSrchTxt.TabIndex = 57;
            this.ClientNameSrchTxt.UseCustomBackColor = true;
            this.ClientNameSrchTxt.UseCustomForeColor = true;
            this.ClientNameSrchTxt.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(333, 33);
            this.metroLabel1.TabIndex = 56;
            this.metroLabel1.Text = "Imię:";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // ClientControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.ClientTabTable);
            this.Name = "ClientControl";
            this.Size = new System.Drawing.Size(992, 552);
            this.Load += new System.EventHandler(this.ClientTabControl_Load);
            this.ClientTabTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ClientDataGrid)).EndInit();
            this.metroPanel1.ResumeLayout(false);
            this.DownSplitContainer.Panel1.ResumeLayout(false);
            this.DownSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DownSplitContainer)).EndInit();
            this.DownSplitContainer.ResumeLayout(false);
            this.BtnTableLayout.ResumeLayout(false);
            this.FormPanel.ResumeLayout(false);
            this.FormPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridViewTextBoxColumn firstNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn telephoneDataGridViewTextBoxColumn;
        private System.Windows.Forms.TableLayoutPanel ClientTabTable;
        private System.Windows.Forms.DataGridView ClientDataGrid;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private System.Windows.Forms.SplitContainer DownSplitContainer;
        private System.Windows.Forms.TableLayoutPanel BtnTableLayout;
        private MetroFramework.Controls.MetroTile SearchClientBtn;
        private MetroFramework.Controls.MetroTile OperationClientBtn;
        private System.Windows.Forms.TableLayoutPanel FormPanel;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox ClientPeselSrchTxt;
        private MetroFramework.Controls.MetroTextBox ClientPhoneSrchTxt;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox ClientMailSrchTxt;
        private MetroFramework.Controls.MetroTextBox ClientSurnameSrchTxt;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox ClientNameSrchTxt;
        private MetroFramework.Controls.MetroLabel metroLabel1;
    }
}
