﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Specialized;
using MetroFramework.Controls;
namespace GymAppWF
{
    class FormValidator
    {
        public static Boolean Validate(Control mainControl)
        {
            StringCollection notEmptyControls = Properties.Settings.Default.NotEmptyControls;
            Boolean isCorrect = true;

            foreach (Control c in mainControl.Controls)
            {
                if (c.GetType() == typeof(TextBox) || c.GetType() == typeof(MetroTextBox))
                {
                    if (notEmptyControls.Contains(c.Name) && (String.IsNullOrEmpty(c.Text) || String.IsNullOrWhiteSpace(c.Text)))
                    {
                        c.ForeColor = Color.Indigo;
                        c.BackColor = Color.MistyRose;
                        isCorrect = false;
                    }
                    else
                    {
                        c.ForeColor = Color.Indigo;
                        c.BackColor = Color.White;
                    }
                }

                if (c.GetType() == typeof(MetroComboBox))
                {
                    MetroComboBox combobox = (MetroComboBox)c;
                    if (combobox.SelectedIndex == -1)
                    {
                        isCorrect = false;
                    }
                }

                if (c.GetType() == typeof(MetroDateTime))
                {
                    MetroDateTime calendar = (MetroDateTime)c;
                    if (calendar.Value == null)
                    {
                        isCorrect = false;
                    }
                }
            }

            if (!isCorrect)
            {
                Control[] control = mainControl.Controls.Find("MsgLbl", false);
                if (control.Length == 1)
                {
                    control[0].Text = "Proszę uzupełnić zaznaczone pola!\n";
                    control[0].Visible = true;
                    control[0].Refresh();
                }
            }
            mainControl.Refresh();
            return isCorrect;
        }

        public static String FormatString(String str)
        {
            if (String.IsNullOrEmpty(str) || str.Length < 2) return str;
            return str.First().ToString().ToUpper() + str.Substring(1).ToLower();
        }
    }
}
