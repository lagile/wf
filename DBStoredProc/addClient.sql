SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		LJedrzynski
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE addClient
	--PERSON
	@FirstName nvarchar(20),
	@LastName nvarchar(40),
	@Email nvarchar(254),
	@Telephone nvarchar(12),
	@Pesel nvarchar(11),
	--CLIENT
	--ADDRESS
	@Street nvarchar(70),
	@City nvarchar(50),
	@Country nvarchar(30),
	@PostCode nvarchar(10),
	@HouseNum nvarchar(10),
	@ApartmentNum nvarchar(10),
	@Msg nvarchar(255) OUTPUT,
	@Err smallint OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
	DECLARE @AddressId int
	SELECT TOP 1 @AddressId = Id
	FROM dbo.AddressSet 
	WHERE Street = @Street
	AND City = @City
	AND Country = @Country
	AND PostCode = @PostCode
	AND HouseNum = @HouseNum
	AND ApartmentNum = @ApartmentNum
	IF @AddressId IS NULL
		BEGIN
			INSERT INTO dbo.AddressSet 
			VALUES(@Street, @City, @Country, @PostCode, @HouseNum, @ApartmentNum)
			SET @AddressId = SCOPE_IDENTITY()
		END
	END
	IF (SELECT COUNT(*)
		FROM dbo.PersonSet 
		WHERE Email = @Email	
		OR ((@Pesel IS NULL OR @Pesel = '') OR PESEL = @Pesel)
		OR Telephone = @Telephone) > 0
	BEGIN
		SET @Msg = 'U�ytkownik o nast�puj�cych danych ju� istnieje.'
		SET @Err = -1
		ROLLBACK TRANSACTION
		RETURN
	END
	INSERT INTO dbo.PersonSet
	VALUES(@FirstName, @LastName, @Email, @Telephone, @Pesel, @AddressId)
	INSERT INTO dbo.PersonSet_Client 
	VALUES(SYSDATETIME(), NULL, 0, SCOPE_IDENTITY())
	SET @Msg = 'U�ytkownik zosta� dodany!'
	SET @Err = 0
	COMMIT TRANSACTION
GO