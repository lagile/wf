﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymAppWF
{
    public static class ConnectionManager
    {
        private static String conString = Properties.Settings.Default.conString;

        public static void ExecuteStoredProcedure(String procedureName, List<SqlParameter> parameters, ref Int16 err, ref String msg)
        {
            SqlParameter errParam = new SqlParameter("@Err", SqlDbType.SmallInt, 255);
            SqlParameter msgParam = new SqlParameter("@Msg", SqlDbType.NVarChar, 255);

            try
            {
                using (SqlConnection con = new SqlConnection(conString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = procedureName;
                        foreach (SqlParameter param in parameters)
                        {
                            cmd.Parameters.Add(param);
                        }
                        errParam.Direction = ParameterDirection.Output;
                        msgParam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(msgParam);
                        cmd.Parameters.Add(errParam);
                        cmd.ExecuteNonQuery();
                    }
                }
                err = Convert.ToInt16(errParam.Value);
                msg = msgParam.Value.ToString();
            }
            catch (Exception exc)
            {
                err = -1;
                msg = exc.Message;
            }
        }

        public static DataSet ExecuteQuery(String query, String sourceTable, ref Int16 err, ref String msg)
        {
            DataSet data = new DataSet();
            try
            {
                using (SqlConnection con = new SqlConnection(Properties.Settings.Default.conString))
                {
                    con.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter(
                        query, con))
                    {
                        a.Fill(data, sourceTable);
                    }
                }
            }
            catch (Exception exc)
            {
                msg = exc.Message;
                err = -1;
            }
            return data;
        }

        public static DataSet UpdateData(String query, String sourceTable, DataSet data, ref Int16 err, ref String msg)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Properties.Settings.Default.conString))
                {
                    con.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(query, con))
                    {
                        SqlCommandBuilder objCommandBuilder = new SqlCommandBuilder(adapter);
                        adapter.Update(data, sourceTable);
                    }
                }
            }
            catch (Exception exc)
            {
                msg = exc.Message;
                err = -1;
            }
            return data;
        }


    }
}
