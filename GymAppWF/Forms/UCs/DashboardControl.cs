﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace GymAppWF.Forms.UCs
{
    public partial class DashboardControl : MetroUserControl
    {

        private NewClientControl newClientControl;
        private ClientControl clientTabControl;
        private PassControl passControl;
        private NewPassControl newPassControl;

        public DashboardControl()
        {
            InitializeComponent();
        }

        private void AddClientBtn_Click(object sender, EventArgs e)
        {
            DynamicPanel.Controls.Clear();
            newClientControl = new NewClientControl();
            newClientControl.Dock = DockStyle.Fill;
            DynamicPanel.Controls.Add(newClientControl);
        }

        private void DashBoardTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DashBoardTab.SelectedTab == ClientTab)
            {
                if(clientTabControl == null)
                {
                    clientTabControl = new ClientControl();
                    clientTabControl.Dock = Dock = DockStyle.Fill;
                    ClientPanel.Controls.Add(clientTabControl);
                }
            } 
            else if(DashBoardTab.SelectedTab == PassTab)
            {
                if (passControl == null)
                {
                    passControl = new PassControl();
                    passControl.Dock = DockStyle.Fill;
                    PassPanel.Controls.Add(passControl);
                }
            }
        }

        private void DashboardControl_Load(object sender, EventArgs e)
        {
            DashBoardTab.SelectedTab = Dashboard;
        }

        private void addPassBtn_Click(object sender, EventArgs e)
        {
            DynamicPanel.Controls.Clear();
            newPassControl = new NewPassControl();
            newPassControl.Dock = DockStyle.Fill;
            DynamicPanel.Controls.Add(newPassControl);
        }

        private void Dashboard_Click(object sender, EventArgs e)
        {

        }

        private void ClientPanel_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
