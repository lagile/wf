﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MetroFramework;
using MetroFramework.Forms;

namespace GymAppWF.Forms.UCs
{
    public partial class NewClientControl : UserControl
    {
        public NewClientControl()
        {
            InitializeComponent();
        }
        private void AddClientBtn_Click(object sender, EventArgs e)
        {
            Int16 err = 0;
            string msg = String.Empty;
            MsgLbl.Text = String.Empty;
            if (!FormValidator.Validate(FormPanel)) return;

            ConnectionManager.ExecuteStoredProcedure("addClient", new List<SqlParameter>
              { new SqlParameter("@FirstName", FormValidator.FormatString(ClientNameTxt.Text)),
                new SqlParameter("@LastName", FormValidator.FormatString(ClientSurnameTxt.Text)),
                new SqlParameter("@Email", ClientMailTxt.Text),
                new SqlParameter("@Telephone", ClientPhoneTxt.Text),
                new SqlParameter("@Pesel", ClientPeselTxt.Text),
                new SqlParameter("@Street", FormValidator.FormatString(ClientStreetTxt.Text)),
                new SqlParameter("@City", FormValidator.FormatString(ClientCityTxt.Text)),
                new SqlParameter("@Country", FormValidator.FormatString(ClientCountryTxt.Text)),
                new SqlParameter("@PostCode", ClientPostCodeTxt.Text),
                new SqlParameter("@HouseNum", ClientHouseNumTxt.Text),
                new SqlParameter("@ApartmentNum", ClientApartNumTxt.Text),
            }, ref err, ref msg);

            if (err < 0)
            {
                MetroMessageBox.Show(this.ParentForm, msg, "Upss", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MetroMessageBox.Show(this.ParentForm, msg, "Powodzenie!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Dispose();
            }    
        }
    }
}
