USE [AS_s11567]
GO
/****** Object:  StoredProcedure [dbo].[addPass]    Script Date: 2016-04-15 17:59:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		LJedrzynski	
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[addPass]
	--PERSON
	@ClientId int,
	@PassName nvarchar(255),
	@StartDate DateTime,
	@Msg nvarchar(255) OUTPUT,
	@Err smallint OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
	DECLARE @passDays int
	DECLARE @passDefId int

	SELECT @passDefId = Id, @passDays = DayNum 
	FROM PassDefSet 
	WHERE Name = @PassName;

	IF(SELECT COUNT(*) 
		FROM PersonSet t1, PersonSet_Client t2
		WHERE t1.Id = @ClientId AND t1.Id = t2.Id) > 0 
		BEGIN
			IF @passDefId IS NOT NULL AND @passDays IS NOT NULL
				BEGIN
					INSERT INTO PassSet VALUES (CAST(@ClientId AS varchar(5)) + '-' + @PassName, @StartDate, DATEADD(day ,@passDays , @StartDate), @ClientId, @passDefId, 1)
				END
			ELSE
				BEGIN
					SET @Msg = 'Niema takiej definicji karnetu!'
					SET @Err = -313
					ROLLBACK TRANSACTION
					RETURN
				END
		END
	ELSE
		BEGIN
			SET @Msg = 'Niema takiego klienta!'
			SET @Err = -313
			ROLLBACK TRANSACTION
			RETURN
		END
	
	SET @Msg = 'Karnet został utworzony!'
	SET @Err = 0
	COMMIT TRANSACTION
END
