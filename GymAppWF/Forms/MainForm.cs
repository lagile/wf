﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using GymAppWF.Forms.UCs;

namespace GymAppWF
{
    public partial class MainForm : MetroForm
    {
        private DashboardControl dashboard;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            dashboard = new DashboardControl();
            dashboard.Dock= DockStyle.Fill;
            MainPanel.Controls.Add(dashboard);
        }
    }
}
