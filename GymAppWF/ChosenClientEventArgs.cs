﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymAppWF
{
    public class ChosenClientEventArgs : EventArgs
    {
        private string clientId;

        public ChosenClientEventArgs(string clientId)
        {
            this.clientId = clientId;
        }

        public string ClientId
        {
            get { return clientId; }
        }

    }
}
