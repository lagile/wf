﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace GymAppWF.Forms.UCs
{

    public partial class ClientControl : UserControl
    {
        public event EventHandler<ChosenClientEventArgs> ChooseClient;
        private DataSet clientDataSet;

        public ClientControl()
        {
            InitializeComponent();
        }

        public ClientControl(String clientId)
        {
            InitializeComponent();
            if (!String.IsNullOrEmpty(clientId))
            {
                DataGridViewRow row = ClientDataGrid.Rows
               .Cast<DataGridViewRow>()
               .Where(r => r.Cells["Id"].Value.ToString().Equals(clientId))
               .First();
                row.Selected = true;
            }


            this.Size = new Size(636, 552);
            DownSplitContainer.SplitterDistance = 250;
            OperationClientBtn.Text = "Wybierz";
        }

        private void LoadGrid(String query)
        {
            Int16 err = 0;
            String msg = String.Empty;

            ClientDataGrid.AutoGenerateColumns = true;
            clientDataSet = ConnectionManager.ExecuteQuery(query, "ClientView", ref err, ref msg);
            ClientDataGrid.DataSource = clientDataSet.Tables[0];
            ClientDataGrid.Columns[0].Visible = false;

            if (err < 0)
            {
                MetroMessageBox.Show(this.ParentForm, msg, "Upps!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClientTabControl_Load(object sender, EventArgs e)
        {
            LoadGrid("SELECT * FROM clientView");
        }

        private void OperationClientBtn_Click(object sender, EventArgs e)
        {
            Int16 err = 0;
            String msg = String.Empty;
            if (OperationClientBtn.Text.Equals("Usuń"))
            {
                foreach (DataGridViewRow row in ClientDataGrid.SelectedRows)
                {
                    if (row != null)
                    {
                        ConnectionManager.ExecuteStoredProcedure("deleteClient", new List<SqlParameter>
                        { new SqlParameter("@FirstName", row.Cells[0].Value.ToString()),
                        new SqlParameter("@LastName", row.Cells[1].Value.ToString()),
                        new SqlParameter("@Email", row.Cells[2].Value.ToString()),
                        new SqlParameter("@Telephone", row.Cells[3].Value.ToString()),
                        new SqlParameter("@PESEL", row.Cells[4].Value.ToString())
                        }, ref err, ref msg);

                        LoadGrid("SELECT * FROM clientView");
                    }
                }

                if (err < 0)
                {
                    MetroMessageBox.Show(this.ParentForm, msg, "Upps!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MetroMessageBox.Show(this.ParentForm, "Dane zostały zaaktualizowane", "Sukces!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            else if (OperationClientBtn.Text.Equals("Wybierz"))
            {
                if (ClientDataGrid.SelectedRows.Count > 0)
                {
                    OnClientChoose(new ChosenClientEventArgs(ClientDataGrid.SelectedRows[0].Cells[0].Value.ToString()));
                    this.Dispose();
                }
            }
        }

        protected virtual void OnClientChoose(ChosenClientEventArgs e)
        {
            EventHandler<ChosenClientEventArgs> eh = ChooseClient;
            if (eh != null)
                eh(this, e);
        }

        private void SearchClientBtn_Click(object sender, EventArgs e)
        {
            Int16 err = 0;
            String msg = String.Empty, query = "SELECT * FROM clientView WHERE 1 = 1 ";

            if (!String.IsNullOrEmpty(ClientNameSrchTxt.Text))
            {
                query += " AND Imię = '" + ClientNameSrchTxt.Text + "'";
            }
            if (!String.IsNullOrEmpty(ClientSurnameSrchTxt.Text))
            {
                query += " AND Nazwisko = '" + ClientSurnameSrchTxt.Text + "'";
            }
            if (!String.IsNullOrEmpty(ClientMailSrchTxt.Text))
            {
                query += " AND Email = '" + ClientMailSrchTxt.Text + "'";
            }
            if (!String.IsNullOrEmpty(ClientPhoneSrchTxt.Text))
            {
                query += " AND Telefon = '" + ClientPhoneSrchTxt.Text + "'";
            }
            if (!String.IsNullOrEmpty(ClientPhoneSrchTxt.Text))
            {
                query += " AND Pesel = '" + ClientPeselSrchTxt.Text + "'";
            }

            LoadGrid(query);

            if (err < 0)
            {
                MetroMessageBox.Show(this.ParentForm, msg, "Upps!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CustomizeForSearch()
        {

        }
    }
}
