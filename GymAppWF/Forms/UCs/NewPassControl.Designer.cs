﻿namespace GymAppWF.Forms.UCs
{
    partial class NewPassControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.FormPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ChooseClientBtn = new MetroFramework.Controls.MetroTile();
            this.PassTypesList = new MetroFramework.Controls.MetroComboBox();
            this.MsgLbl = new MetroFramework.Controls.MetroLabel();
            this.CreatePassBtn = new MetroFramework.Controls.MetroTile();
            this.StartDateCalendar = new MetroFramework.Controls.MetroDateTime();
            this.MainPanel = new MetroFramework.Controls.MetroPanel();
            this.FormPanel.SuspendLayout();
            this.MainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel6.Location = new System.Drawing.Point(33, 30);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(116, 25);
            this.metroLabel6.TabIndex = 20;
            this.metroLabel6.Text = "Nowy karnet:";
            this.metroLabel6.UseCustomBackColor = true;
            this.metroLabel6.UseCustomForeColor = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel2.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel2.Location = new System.Drawing.Point(3, 36);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(276, 36);
            this.metroLabel2.TabIndex = 57;
            this.metroLabel2.Text = "Data rozpoczęcia:";
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel3.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel3.Location = new System.Drawing.Point(3, 72);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(276, 36);
            this.metroLabel3.TabIndex = 58;
            this.metroLabel3.Text = "Klient:";
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.metroLabel1.ForeColor = System.Drawing.Color.Indigo;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(276, 36);
            this.metroLabel1.TabIndex = 56;
            this.metroLabel1.Text = "Typ karnetu:";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // FormPanel
            // 
            this.FormPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FormPanel.ColumnCount = 2;
            this.FormPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.FormPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.FormPanel.Controls.Add(this.ChooseClientBtn, 1, 2);
            this.FormPanel.Controls.Add(this.PassTypesList, 1, 0);
            this.FormPanel.Controls.Add(this.MsgLbl, 0, 3);
            this.FormPanel.Controls.Add(this.metroLabel2, 0, 1);
            this.FormPanel.Controls.Add(this.metroLabel3, 0, 2);
            this.FormPanel.Controls.Add(this.metroLabel1, 0, 0);
            this.FormPanel.Controls.Add(this.CreatePassBtn, 1, 3);
            this.FormPanel.Controls.Add(this.StartDateCalendar, 1, 1);
            this.FormPanel.Location = new System.Drawing.Point(33, 71);
            this.FormPanel.Name = "FormPanel";
            this.FormPanel.RowCount = 4;
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946742F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.946743F));
            this.FormPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.FormPanel.Size = new System.Drawing.Size(565, 146);
            this.FormPanel.TabIndex = 21;
            // 
            // ChooseClientBtn
            // 
            this.ChooseClientBtn.ActiveControl = null;
            this.ChooseClientBtn.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.ChooseClientBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChooseClientBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ChooseClientBtn.Location = new System.Drawing.Point(285, 75);
            this.ChooseClientBtn.Name = "ChooseClientBtn";
            this.ChooseClientBtn.Size = new System.Drawing.Size(277, 30);
            this.ChooseClientBtn.TabIndex = 63;
            this.ChooseClientBtn.Text = "Wybierz klienta";
            this.ChooseClientBtn.UseCustomBackColor = true;
            this.ChooseClientBtn.UseCustomForeColor = true;
            this.ChooseClientBtn.UseSelectable = true;
            this.ChooseClientBtn.Click += new System.EventHandler(this.ChooseClientBtn_Click);
            // 
            // PassTypesList
            // 
            this.PassTypesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PassTypesList.FormattingEnabled = true;
            this.PassTypesList.ItemHeight = 24;
            this.PassTypesList.Items.AddRange(new object[] {
            ""});
            this.PassTypesList.Location = new System.Drawing.Point(285, 3);
            this.PassTypesList.Name = "PassTypesList";
            this.PassTypesList.Size = new System.Drawing.Size(277, 30);
            this.PassTypesList.TabIndex = 23;
            this.PassTypesList.UseSelectable = true;
            // 
            // MsgLbl
            // 
            this.MsgLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MsgLbl.AutoSize = true;
            this.MsgLbl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.MsgLbl.ForeColor = System.Drawing.Color.Salmon;
            this.MsgLbl.Location = new System.Drawing.Point(3, 108);
            this.MsgLbl.Name = "MsgLbl";
            this.MsgLbl.Size = new System.Drawing.Size(276, 38);
            this.MsgLbl.TabIndex = 60;
            this.MsgLbl.UseCustomBackColor = true;
            this.MsgLbl.UseCustomForeColor = true;
            // 
            // CreatePassBtn
            // 
            this.CreatePassBtn.ActiveControl = null;
            this.CreatePassBtn.BackColor = System.Drawing.Color.Indigo;
            this.CreatePassBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CreatePassBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.CreatePassBtn.Location = new System.Drawing.Point(285, 111);
            this.CreatePassBtn.Name = "CreatePassBtn";
            this.CreatePassBtn.Size = new System.Drawing.Size(277, 32);
            this.CreatePassBtn.TabIndex = 61;
            this.CreatePassBtn.Text = "Utwórz karnet";
            this.CreatePassBtn.UseCustomBackColor = true;
            this.CreatePassBtn.UseCustomForeColor = true;
            this.CreatePassBtn.UseSelectable = true;
            this.CreatePassBtn.Click += new System.EventHandler(this.CreatePassBtn_Click);
            // 
            // StartDateCalendar
            // 
            this.StartDateCalendar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StartDateCalendar.Location = new System.Drawing.Point(285, 39);
            this.StartDateCalendar.MinimumSize = new System.Drawing.Size(0, 30);
            this.StartDateCalendar.Name = "StartDateCalendar";
            this.StartDateCalendar.Size = new System.Drawing.Size(277, 30);
            this.StartDateCalendar.TabIndex = 62;
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.FormPanel);
            this.MainPanel.Controls.Add(this.metroLabel6);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.HorizontalScrollbarBarColor = true;
            this.MainPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.MainPanel.HorizontalScrollbarSize = 10;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(636, 557);
            this.MainPanel.TabIndex = 2;
            this.MainPanel.VerticalScrollbarBarColor = true;
            this.MainPanel.VerticalScrollbarHighlightOnWheel = false;
            this.MainPanel.VerticalScrollbarSize = 10;
            // 
            // NewPassControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainPanel);
            this.Name = "NewPassControl";
            this.Size = new System.Drawing.Size(636, 557);
            this.Load += new System.EventHandler(this.NewPassControl_Load);
            this.FormPanel.ResumeLayout(false);
            this.FormPanel.PerformLayout();
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.TableLayoutPanel FormPanel;
        private MetroFramework.Controls.MetroPanel MainPanel;
        private MetroFramework.Controls.MetroLabel MsgLbl;
        private MetroFramework.Controls.MetroComboBox PassTypesList;
        private MetroFramework.Controls.MetroTile CreatePassBtn;
        private MetroFramework.Controls.MetroDateTime StartDateCalendar;
        private MetroFramework.Controls.MetroTile ChooseClientBtn;
    }
}
