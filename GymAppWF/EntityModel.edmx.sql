
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/06/2016 19:06:49
-- Generated from EDMX file: c:\users\lukasz\documents\visual studio 2015\Projects\GymAppWF\GymAppWF\EntityModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [AS_s11567];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'PersonSet'
CREATE TABLE [dbo].[PersonSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Telephone] nvarchar(max)  NOT NULL,
    [PESEL] nvarchar(max)  NULL,
    [Person2Address] int  NOT NULL
);
GO

-- Creating table 'AddressSet'
CREATE TABLE [dbo].[AddressSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Street] nvarchar(max)  NOT NULL,
    [City] nvarchar(max)  NOT NULL,
    [Country] nvarchar(max)  NOT NULL,
    [PostCode] nvarchar(max)  NOT NULL,
    [HouseNum] nvarchar(max)  NOT NULL,
    [ApartmentNum] nvarchar(max)  NULL
);
GO

-- Creating table 'PassSet'
CREATE TABLE [dbo].[PassSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PassId] nvarchar(max)  NOT NULL,
    [StartDate] datetime  NULL,
    [ExpDate] datetime  NULL,
    [Pass2Client] int  NOT NULL,
    [Pass2Def] int  NOT NULL,
    [Valid] tinyint  NOT NULL
);
GO

-- Creating table 'EntrySet'
CREATE TABLE [dbo].[EntrySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EntryDate] datetime  NOT NULL,
    [Entry2Pass] int  NOT NULL
);
GO

-- Creating table 'PassDefSet'
CREATE TABLE [dbo].[PassDefSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Price] smallint  NOT NULL,
    [DayNum] smallint  NULL
);
GO

-- Creating table 'PersonSet_Client'
CREATE TABLE [dbo].[PersonSet_Client] (
    [SignUpDate] datetime  NULL,
    [SignOffDate] datetime  NULL,
    [VIP] tinyint  NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'PersonSet_Employee'
CREATE TABLE [dbo].[PersonSet_Employee] (
    [HireDate] datetime  NULL,
    [Salary] smallint  NULL,
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'PersonSet'
ALTER TABLE [dbo].[PersonSet]
ADD CONSTRAINT [PK_PersonSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AddressSet'
ALTER TABLE [dbo].[AddressSet]
ADD CONSTRAINT [PK_AddressSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PassSet'
ALTER TABLE [dbo].[PassSet]
ADD CONSTRAINT [PK_PassSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EntrySet'
ALTER TABLE [dbo].[EntrySet]
ADD CONSTRAINT [PK_EntrySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PassDefSet'
ALTER TABLE [dbo].[PassDefSet]
ADD CONSTRAINT [PK_PassDefSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PersonSet_Client'
ALTER TABLE [dbo].[PersonSet_Client]
ADD CONSTRAINT [PK_PersonSet_Client]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PersonSet_Employee'
ALTER TABLE [dbo].[PersonSet_Employee]
ADD CONSTRAINT [PK_PersonSet_Employee]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Person2Address] in table 'PersonSet'
ALTER TABLE [dbo].[PersonSet]
ADD CONSTRAINT [FK_AddressPerson]
    FOREIGN KEY ([Person2Address])
    REFERENCES [dbo].[AddressSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AddressPerson'
CREATE INDEX [IX_FK_AddressPerson]
ON [dbo].[PersonSet]
    ([Person2Address]);
GO

-- Creating foreign key on [Pass2Def] in table 'PassSet'
ALTER TABLE [dbo].[PassSet]
ADD CONSTRAINT [FK_PassPassDef]
    FOREIGN KEY ([Pass2Def])
    REFERENCES [dbo].[PassDefSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PassPassDef'
CREATE INDEX [IX_FK_PassPassDef]
ON [dbo].[PassSet]
    ([Pass2Def]);
GO

-- Creating foreign key on [Pass2Client] in table 'PassSet'
ALTER TABLE [dbo].[PassSet]
ADD CONSTRAINT [FK_PassClient]
    FOREIGN KEY ([Pass2Client])
    REFERENCES [dbo].[PersonSet_Client]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PassClient'
CREATE INDEX [IX_FK_PassClient]
ON [dbo].[PassSet]
    ([Pass2Client]);
GO

-- Creating foreign key on [Entry2Pass] in table 'EntrySet'
ALTER TABLE [dbo].[EntrySet]
ADD CONSTRAINT [FK_PassEntry]
    FOREIGN KEY ([Entry2Pass])
    REFERENCES [dbo].[PassSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PassEntry'
CREATE INDEX [IX_FK_PassEntry]
ON [dbo].[EntrySet]
    ([Entry2Pass]);
GO

-- Creating foreign key on [Id] in table 'PersonSet_Client'
ALTER TABLE [dbo].[PersonSet_Client]
ADD CONSTRAINT [FK_Client_inherits_Person]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[PersonSet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'PersonSet_Employee'
ALTER TABLE [dbo].[PersonSet_Employee]
ADD CONSTRAINT [FK_Employee_inherits_Person]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[PersonSet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------