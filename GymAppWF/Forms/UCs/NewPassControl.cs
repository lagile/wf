﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace GymAppWF.Forms.UCs
{
    public partial class NewPassControl : UserControl
    {
        private ClientControl clientControl;
        private String passClientId;
        public NewPassControl()
        {
            InitializeComponent();
        }

        private void GetClientId(object sender, ChosenClientEventArgs e)
        {
            if (e.ClientId != null)
            {
                passClientId = e.ClientId;
                ChooseClientBtn.Text = "Wybrano klienta";
                ChooseClientBtn.BackColor = Color.MediumAquamarine;
                MsgLbl.Text = "";
            }
        }

        private void ChooseClientBtn_Click(object sender, EventArgs e)
        {
            clientControl = new ClientControl(passClientId);
            clientControl.ChooseClient += new EventHandler<ChosenClientEventArgs>(GetClientId);
            clientControl.Dock = Dock = DockStyle.Fill;
            clientControl.CustomizeForSearch();

            if (!MainPanel.Controls.Contains(clientControl))
            {
                MainPanel.Parent.Controls.Add(clientControl);
                clientControl.BringToFront();
            }
        }

        private void CreatePassBtn_Click(object sender, EventArgs e)
        {
            Int16 err = 0;
            String msg = String.Empty;

            MsgLbl.Text = "";

            if (!FormValidator.Validate(FormPanel)) return;

            if (passClientId == null)
            {
                MsgLbl.Text = "Proszę wybrać klienta!";
                return;
            }

            if (StartDateCalendar.Value.Date < DateTime.Now.Date)
            {
                MsgLbl.Text = "Data startu karnetu nie może być w przeszłości!";
                return;
            }

            ConnectionManager.ExecuteStoredProcedure("addPass", new List<SqlParameter>
                        {   new SqlParameter("@ClientId", Int32.Parse(passClientId)),
                            new SqlParameter("@PassName", PassTypesList.Text),
                            new SqlParameter("@StartDate", StartDateCalendar.Value.Date)
                        }, ref err, ref msg);

            if (err < 0)
            {
                MetroMessageBox.Show(this.ParentForm, msg, "Upps!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MetroMessageBox.Show(this.ParentForm, "Karnet został utworzony!", "Sukces!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.Dispose();
        }

        private void NewPassControl_Load(object sender, EventArgs e)
        {
            Int16 err = 0;
            String msg = String.Empty;

            DataSet dataSet = ConnectionManager.ExecuteQuery("SELECT Id,name FROM dbo.PassDefSet", "PassDefSet", ref err, ref msg);

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                PassTypesList.Items.Add(Convert.ToString(dataSet.Tables[0].Rows[i]["name"]));
            }

            if (err < 0)
            {
                MetroMessageBox.Show(this.ParentForm, msg, "Upps!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
